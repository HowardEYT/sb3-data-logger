from PyQt5.QtWidgets import (QWidget, QMainWindow, QVBoxLayout, QLineEdit,
                                QTabWidget, QDesktopWidget, QPushButton,
                                QLabel, QGroupBox, QFileDialog, QSlider, QFormLayout)
from PyQt5.Qt import QStyleOption, QStyle
from PyQt5.QtGui import QPainter, QIntValidator
from PyQt5.QtCore import pyqtSignal
import pyqtgraph as pg
import pyqtgraph.exporters as pgexp
import numpy as np
import sys

class BaseWidget(QWidget):
    """
     A base widget reimplemented to allow custom styling.
     Pretty much identical to a normal QWidget
    """
    def __init__(self, *arg, **kwarg):
        """
        Reimplemented from QWidget
        """
        super().__init__(*arg, **kwarg)
        self.setObjectName('subWidget')

    def paintEvent(self, evt):
        """
        Reimplemented from QWidget.paintEvent()
        """
        super().paintEvent(evt)
        opt = QStyleOption()
        opt.initFrom(self)
        p = QPainter(self)
        s = self.style()
        s.drawPrimitive(QStyle.PE_Widget, opt, p, self)

class connection_panel(BaseWidget):
    """
    The panel which has a button to connect to the Arduino.
    Shows the status of the connection and latest data received.
    """
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.connect_btn = QPushButton('Connect to Arduino', self)

        self.status_box = QGroupBox('Status', self)
        self.status_layout = QVBoxLayout(self.status_box)
        self.status_label = QLabel('Offline')
        self.status_layout.addWidget(self.status_label)
        self.current_data_label = QLabel('No Data')
        self.status_layout.addWidget(self.current_data_label)

        self.layout.addWidget(self.connect_btn)
        self.layout.addWidget(self.status_box)

class data_panel(BaseWidget):
    """
    The panel which contains a button to show live data.
    Contains a slider to vary brightness.
    Contains LineEdit to vary the sample time.
    """
    sampletimeChanged = pyqtSignal(int, str)
    brightnessChanged = pyqtSignal(int, str)

    def __init__(self):
        super().__init__()
        self.layout = QFormLayout(self)
        self.live_btn = QPushButton('Off', self)

        self.brightness_slider = QSlider(self)
        self.brightness_slider.setMinimum(0)
        self.brightness_slider.setMaximum(255)
        self.brightness_slider.setOrientation(1)
        self.brightness_slider.setTracking(False)
        self.brightness_slider.valueChanged.connect(self.display_val)
        self.brightness_label = QLabel('Brightness: 0', self)

        self.sample_time = QLineEdit('3', self)
        validator = QIntValidator(self.sample_time)
        self.sample_time.setValidator(validator)
        self.sample_time.editingFinished.connect(self.set_sample_time)

        self.layout.addRow(QLabel("Live Data: "),self.live_btn)
        self.layout.addRow(self.brightness_label, self.brightness_slider)
        self.layout.addRow(QLabel("Sample Time(secs): "),self.sample_time)

    def display_val(self, val):
        """
        Displays the brightness set and emits a signal brightnessChanged
        """
        self.brightness_label.setText('Brightness: {:.2f}'.format(val/255))
        self.brightnessChanged.emit(val, 'lcd')

    def set_sample_time(self):
        """
        Sends the set sample time out through a signal sampletimeChanged
        """
        string = self.sample_time.text()
        self.sampletimeChanged.emit(int(string) * 1000, 'sdc')

    def toggle_live_status(self, status):
        if status:
            self.live_btn.setText('On')
        else:
            self.live_btn.setText('Off')

class export_panel(BaseWidget):
    """
    The panel which contains three buttons to load csv files, save csv file,
    and export to png.
    These buttons must be disabled if showing live data.
    """
    savefile = pyqtSignal(str)
    imgfile = pyqtSignal(str)
    loadfile = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.save_btn = QPushButton('Save data', self)
        self.export_btn = QPushButton('Export graph', self)
        self.load_data_btn = QPushButton('Load csv data', self)

        self.layout.addWidget(self.load_data_btn)
        self.layout.addWidget(self.save_btn)
        self.layout.addWidget(self.export_btn)


        # Currently, there are currently three separate FileDialog for each function
        # Can optimise by using one and setting the options each time.
        self.loadfilebox = QFileDialog(self)
        self.loadfilebox.setModal(True)
        self.loadfilebox.setAcceptMode(0)
        self.loadfilebox.setFileMode(QFileDialog.ExistingFile)
        self.loadfilebox.setNameFilters(["csv (*.csv *.CSV)"])

        self.savefilebox = QFileDialog(self)
        self.savefilebox.setModal(True)
        self.savefilebox.setAcceptMode(1)
        self.savefilebox.setFileMode(QFileDialog.AnyFile)
        self.savefilebox.setNameFilters(["csv (*.csv)"])
        self.savefilebox.setDefaultSuffix(".csv")

        self.imgfilebox = QFileDialog(self)
        self.imgfilebox.setModal(True)
        self.imgfilebox.setAcceptMode(1)
        self.imgfilebox.setFileMode(QFileDialog.AnyFile)
        self.imgfilebox.setNameFilters(["png (*.png)"])
        self.imgfilebox.setDefaultSuffix(".png")

        self.load_data_btn.clicked.connect(self.get_loadfile_dir)
        self.save_btn.clicked.connect(self.get_savefile_dir)
        self.export_btn.clicked.connect(self.get_imgfile_dir)

    def get_savefile_dir(self):
        """
        Get the save file directory
        """
        fileName = []
        self.savefilebox.show()
        if self.savefilebox.exec():
            fileName = self.savefilebox.selectedFiles()

        if fileName:
            print(fileName[0])
            self.savefile.emit(fileName[0])

    def get_imgfile_dir(self):
        """
        Get the save image directory
        """
        fileName = []
        self.imgfilebox.show()
        if self.imgfilebox.exec():
            fileName = self.imgfilebox.selectedFiles()
        if fileName:
            print(fileName[0])
            self.imgfile.emit(fileName[0])

    def get_loadfile_dir(self):
        """
        Get the load file directory
        """
        fileName = []
        self.loadfilebox.show()
        if self.loadfilebox.exec():
            fileName = self.loadfilebox.selectedFiles()

        if fileName:
            print(fileName[0])
            self.loadfile.emit(fileName[0])

    def disenable_buttons(self, state):
        """
        Disable/Enable the buttons
        """
        self.load_data_btn.setEnabled(state)
        self.save_btn.setEnabled(state)
        self.export_btn.setEnabled(state)

class plot_visualiser(BaseWidget):
    """
    The plot widget to display the data.
    """
    def __init__(self):
        super().__init__()

        self.layout = QVBoxLayout(self)

        self.plot_area = pg.PlotWidget(self)
        self.layout.addWidget(self.plot_area,3)

        self.scroll_area = pg.PlotWidget(self)
        self.layout.addWidget(self.scroll_area,1)

        self.p1 = self.plot_area.plotItem
        self.s1 = self.scroll_area.plotItem

        self.p1.setLabel('bottom','Time(s)')
        self.p1.setLabel('left', 'Temperature', color='#00ff00')
        self.p1.setTitle(' ')
        # Create a new ViewBox, link the right axis to its coordinate system
        self.p2 = pg.ViewBox()
        self.p1.showAxis('right')
        self.p1.scene().addItem(self.p2)
        self.p1.getAxis('right').linkToView(self.p2)
        self.p2.setXLink(self.p1)
        self.p1.getAxis('right').setLabel('Humidity', color='#0000ff')
        self.p1.hideButtons()
        self.p1.setMenuEnabled(False)
        #self.p1.disableAuto

        # Array to contain the data, may not be needed
        self.sensor_datas = []

        self.updateViews()
        self.p1.vb.sigResized.connect(self.updateViews)

        # Add a temperature line to the first viewbox p1
        self.temp_line = self.p1.plot([0,1,2,3,4,5],[1,2,4,8,16,32], pen='g')

        # Add a humidity line to the second viewbox p2
        self.humid_line = pg.PlotDataItem([0,1,2,3,4,5],[10,20,40,80,40,20], pen='b')
        self.p2.addItem(self.humid_line)

        #self.s1.hideAxis('left')
        self.s1.hideAxis('bottom')
        self.s1.setMenuEnabled(False)
        self.scroll_vb = self.s1.getViewBox()
        self.scroll_vb.setMouseEnabled(False,False)
        self.temp_dupe = self.s1.plot([0,1,2,3,4,5],[1,2,4,8,16,32], pen='g')
        self.humid_dupe = self.s1.plot([0,1,2,3,4,5],[10,20,40,80,40,20], pen='b')
        self.s1.setLimits(xMin = 0, xMax = 5)
        self.scroll_box = pg.LinearRegionItem([0,1])
        self.scroll_box.setBounds([0, 5])
        self.scroll_box.setZValue(-10)
        self.s1.addItem(self.scroll_box)

        self.scroll_box.sigRegionChanged.connect(self.updatePlot)
        self.p1.sigXRangeChanged.connect(self.updateRegion)

        self.p1.setRange(yRange = (0,100))
        self.p2.setRange(yRange = (0,100))
        self.center_scrollbox([0,1,2,3,4,5])

    def live_update_data(self, data):
        """
        Get the data and process them.
        """
        data = [float(val) for val in data]
        self.sensor_datas.append(data)
        data_array = np.array(self.sensor_datas)
        self.update_lines(data_array[:,0], data_array[:,1], data_array[:,2])
        scrollbox_region =  self.scroll_box.getRegion()
        scrollbox_width = abs(scrollbox_region[1] - scrollbox_region[0])
        self.scroll_box.setRegion([data_array[-1,2]-scrollbox_width, data_array[-1,2]])

    def update_lines(self, temp, humid, time):
        self.temp_line.setData(x = time, y = temp)
        self.humid_line.setData(x = time, y = humid)
        self.temp_dupe.setData(x = time, y = temp)
        self.humid_dupe.setData(x = time, y = humid)
        self.scroll_box.setBounds([min(time), max(time)])
        self.s1.setLimits(xMin = min(time), xMax = max(time))
        #self.p1.setRange()

    def center_scrollbox(self, time, portion = 0.5):
        center = sum(time)/len(time)
        width = max(time) - min(time)
        one_side_width = portion/2 * width
        self.scroll_box.setRegion([center - one_side_width, center + one_side_width])

    def get_line_datas(self):
        time, temp  = self.temp_line.getData()
        _, humid = self.humid_line.getData()
        return temp,humid,time

    def clear_line_datas(self):
        self.temp_line.clear()
        self.humid_line.clear()
        self.temp_dupe.clear()
        self.humid_dupe.clear()

    def updateViews(self):
        ## view has resized; update auxiliary views to match
        self.p2.setGeometry(self.p1.vb.sceneBoundingRect())
        ## need to re-update linked axes since this was called
        ## incorrectly while views had different shapes.
        ## (probably this should be handled in ViewBox.resizeEvent)
        ## Handle view resizing
        self.p2.linkedViewChanged(self.p1.vb, self.p2.XAxis)

    def updatePlot(self):
        self.p1.setXRange(*self.scroll_box.getRegion(), padding=0)

    def updateRegion(self):
        self.scroll_box.setRegion(self.p1.getViewBox().viewRange()[0])

    def export_graph(self, filename):
        exporter = pgexp.ImageExporter(self.p1)

        # set export parameters if needed
        exporter.params.param('width').setValue(self.p1.viewGeometry().width(),
                                                blockSignal=exporter.widthChanged)
        exporter.params.param('height').setValue(self.p1.viewGeometry().height(),
                                                blockSignal=exporter.heightChanged)

        # save to file
        exporter.export(filename)
        print('Export done!')


#if __name__ == '__main__':
#    pass
