'''
The Main file, install the requirements using the requirements.txt with pip.
pip install -r requirements.txt

The file constructs the main body of the program and handles the signal connection
between widgets.
'''

from PyQt5.QtWidgets import (QWidget, QMainWindow, QHBoxLayout, QVBoxLayout, QTextEdit,
                                QTabWidget, QDesktopWidget)
from PyQt5.Qt import QTimer, QApplication
from PyQt5.QtCore import pyqtSignal
import pyqtgraph as pg

import sys, multiprocessing, serial

from datalogger_widgets import (data_panel, export_panel,
                                connection_panel, plot_visualiser)

from BT_thread import BTthread

from csv_handler import read_sensor_datafile, export_sensor_datafile

WIDTH = 900         # Window width
HEIGHT = 600        # Window height

class DataLogger(QWidget):

    def __init__(self):
        """
        The construction of the program
        """
        super().__init__()

        # Set window parameter
        self.setGeometry(400,300,WIDTH,HEIGHT)
        self.setWindowTitle('Datalogger')

        # Set up the Bluetooth Thread variable and Queue
        self.thread = None # None means no thread initiated
        self.queue = multiprocessing.Queue()

        # Set up the layout of the program
        main_layout = QVBoxLayout(self)

        # Add the plot visualiser
        self.plot_figure = plot_visualiser()
        main_layout.addWidget(self.plot_figure)

        # Add the bottom control panel
        self.control_panel = QHBoxLayout(self)
        main_layout.addLayout(self.control_panel)

        # The control panel consists of the three widgets as below
        self.arduino_widget = connection_panel()
        self.data_widget = data_panel()
        self.export_widget = export_panel()
        self.control_panel.addWidget(self.arduino_widget,3)
        self.control_panel.addWidget(self.data_widget,2)
        self.control_panel.addWidget(self.export_widget,1)

        # Create the timer to periodically process the incoming data
        self.update_timer = QTimer(self)
        self.update_timer.setInterval(1000)
        self.update_timer.timeout.connect(self.processIncoming)

        # Connect the widgets signals
        self.signal_connections()

        # Experimental styling
        self.setStyleSheet('''
        .QWidget{
            background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                stop:0 #eee, stop:1 #ccc);
            border: 1px solid #777;
            width: 13px;
            margin-top: 2px;
            margin-bottom: 2px;
            border-radius: 4px;
        }
        .QGroupBox{
                border: 1px solid black;
                margin-top: 0.5em;
                font: italic;
        }
        .QGroupBox::title {
                top: -6px;
                left: 10px;
        }
        .QWidget #subWidget{
                background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                    stop:0 #eee, stop:1 #ccc);
                border: 1px solid #777;
                width: 13px;
                margin-top: 2px;
                margin-bottom: 2px;
                border-radius: 4px;
            }
        ''')


        # Center and show window
        self.adjust_position()
        self.setFocus()
        self.show()

    def adjust_position(self):
        """
        If it has a parent, adjust its position to be slightly out of the parent
        window towards the left
        """
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def signal_connections(self):
        """
        Connect the signals from to the individual widgets
        """
        self.export_widget.loadfile.connect(self.load_in_data)
        self.export_widget.savefile.connect(self.save_data_csv)
        self.export_widget.imgfile.connect(self.plot_figure.export_graph)
        self.arduino_widget.connect_btn.clicked.connect(self.connect_to_Arduino)
        self.data_widget.live_btn.clicked.connect(self.receive_live_data)

    def load_in_data(self, filename):
        """
        Process the csv file loaded in
        """
        temp,humid,time = read_sensor_datafile(filename)
        self.plot_figure.update_lines(temp,humid,time)
        self.plot_figure.center_scrollbox(time)

    def save_data_csv(self,filename):
        """
        Save the csv file to the location selected
        """
        temp,humid,time = self.plot_figure.get_line_datas()
        export_sensor_datafile(temp,humid,time, filename)
        print('Saving Done!')

    def connect_to_Arduino(self):
        """
        Creates the BT Thread to connect to the Arduino
        """
        if not self.thread:
            # Disable loading/saving data
            self.export_widget.disenable_buttons(False)

            self.arduino_widget.status_label.setText('Connecting...')
            try:
                # Set up the Thread for the BT connection
                self.thread = BTthread()
                self.thread.bt_status.connect(self.print_bt_status)
                self.thread.data_out.connect(self.put_data_in_queue)
                self.thread.finished.connect(self.stop_btthread)
                self.thread.start()
                self.receive_live_data()
            except:
                print('Cannot start Thread', sys.exc_info()[1])
                return
            print('Thread Start!')

            self.data_widget.sampletimeChanged.connect(self.thread.send_value_to_BT)
            self.data_widget.brightnessChanged.connect(self.thread.send_value_to_BT)
            self.data_widget.brightness_slider.setValue(128)

            self.plot_figure.clear_line_datas()
            self.arduino_widget.connect_btn.setText('Disconnect from Arduino')
        else:
            self.thread.stop()
            self.arduino_widget.connect_btn.setText('Connect to Arduino')

        self.arduino_widget.connect_btn.setDisabled(True)

    def stop_btthread(self):
        """
        Stop and delete the BT Thread
        """
        self.thread.toggle_run(False)
        self.data_widget.toggle_live_status(False)
        if self.update_timer.isActive():
            self.update_timer.stop()
        self.export_widget.disenable_buttons(True)
        print('Stop!')
        self.thread.deleteLater()
        self.thread = None
        self.arduino_widget.connect_btn.setText('Connect to Arduino')
        self.arduino_widget.connect_btn.setEnabled(True)

    def put_data_in_queue(self,data):
        """
        Put received data into the queue
        """
        self.queue.put(data)

    def receive_live_data(self):
        """
        Toggle between reading live data or not
        """
        if self.thread:
            if not self.thread.running.value:
                self.thread.toggle_run(True)
                self.update_timer.start()
                print('Running!')
                self.export_widget.disenable_buttons(False)
                self.data_widget.toggle_live_status(True)
                self.plot_figure.clear_line_datas()
            else:
                self.thread.toggle_run(False)
                self.update_timer.stop()
                print('Pausing!')
                self.export_widget.disenable_buttons(True)
                self.data_widget.toggle_live_status(False)

    def print_bt_status(self, msg, code):
        """
        Print any received status. Stop the BT Thread if it is an error
        """
        self.arduino_widget.status_label.setText('BT Status: ' + msg)
        if code == -1:
            print('Error encountered!')
            self.thread.stop()
            self.arduino_widget.connect_btn.setEnabled(True)
        elif code == 1:
            self.arduino_widget.connect_btn.setEnabled(True)

    def processIncoming(self):
        """
        Process the data inside the queue, if any.
        """
        while self.queue.qsize():
            try:
                values = self.queue.get(0).strip('\n')
                value_list = values.split(',')
                if len(value_list) == 3:
                    self.arduino_widget.current_data_label.setText(values)
                    self.plot_figure.live_update_data(value_list)
            except:
                print("Unexpected error:", sys.exc_info()[0])

    def closeEvent(self, ev):
        """
        Reimplemented. At program closing, make sure the thread is close.
        """
        try:
            self.thread.stop()
        except:
            pass

if __name__ == "__main__":
    app = 0
    app = QApplication(sys.argv)

    root  = DataLogger()

    sys.exit(app.exec_())
