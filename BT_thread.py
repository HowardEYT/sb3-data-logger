from PyQt5.QtCore import pyqtSignal, QThread
import sys, bluetooth, multiprocessing

BT_ADDRESS = "00:15:83:35:5B:DA"

class BTthread(QThread):
    bt_status = pyqtSignal(str, bool)
    data_out = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.bt_socket = None
        self.running = multiprocessing.Value('i', 0)
        self.finished.connect(self.stop_BT)

        self.held_data = ''
        self.stop_thread = False

    def stop(self):
        self.stop_thread = True

    def toggle_run(self, state):
        if state:
            self.running.value = 1
        else:
            self.running.value = 0

    def connect_to_BT(self):
        try:
            self.bt_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            self.bt_socket.connect((BT_ADDRESS,1))
            self.bt_socket.settimeout(600)
            self.bt_status.emit('BT Connected!', 1)
            return True
        except bluetooth.BluetoothError:
            self.stop_BT()
            self.bt_status.emit('Connection Failed!', -1)
            return False

    def stop_BT(self):
        if self.bt_socket:
            try:
                self.bt_socket.close()
            except:
                pass
            self.bt_socket = None

    def send_value_to_BT(self, val, header):
        """
        Sends values to the Bluetooth. Used for setting brightness and sample time.
        """
        try:
            if self.bt_socket:
                self.bt_socket.send(bytes(header + str(val), 'UTF-8'))
                print(header + str(val))
        except:
            print('No value sent!')

    def run(self):
        """
        Main loop of the thread to run in non-blocking mode. It attempts to read data from the Bluetooth.
        Any error encounter during reading will emit an error signal and stop the loop.
        Data will not be sent out unless it reads the \n at the end.
        """
        if not self.connect_to_BT():
            return

        while True:
            try:
                data_chunk = self.bt_socket.recv(1024).decode('ascii')
            except:
                error = str(sys.exc_info()[1])
                print('Bad things happend!', error)
                data_chunk = None
                self.bt_status.emit(str(error), -1)
                self.stop_BT()
                break

            if data_chunk:
                self.held_data = self.held_data + data_chunk
                if data_chunk[-2:] == '\r\n':
                    if self.running.value:
                        self.data_out.emit(self.held_data)
                    self.held_data = ''

            if self.stop_thread:
                break
