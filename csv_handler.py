"""
This file contains the functions to read and save csv files.
The data is processing into temperature, humidity, and time, in that order.
The functions are hopefully self-explanatory.
"""

import csv
import numpy as np

def read_sensor_datafile(filename):
    data = []
    with open(filename,newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in csvreader:
            row = [float(num) for num in row]
            data.append(row)

    data = np.array(data)
    temp = data[:,0]
    humid = data[:,1]
    time = data[:,2]

    return temp,humid,time

def export_sensor_datafile(temp, humid, time, filename):
    rows = len(temp)
    temp = temp.reshape(rows,1)
    humid = humid.reshape(rows,1)
    time = time.reshape(rows,1)
    try:
        data = np.concatenate((temp, humid, time), axis = 1)
    except:
        print('Cannot concat array. Check the dimensions of the arrays')
        return -1

    with open(filename,'w',newline='') as csvfile:
        csvwriter= csv.writer(csvfile, delimiter=',', quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerows(data)

if __name__ == "__main__":
    export_sensor_datafile([1,2,3],[3,3,4],[1,5,3],'test.csv')
