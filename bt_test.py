import bluetooth
import subprocess

hostMACAddress = "00:15:83:35:5B:DA" # The MAC address of a Bluetooth adapter on the server. The server might have multiple Bluetooth adapters.
port = 1
size = 1024

success = True

s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
#s.bind((hostMACAddress, port))
#s.listen(backlog)
s.connect((hostMACAddress, port))
s.settimeout(10.0)
try:
    #client, clientInfo = s.accept()
    #full_data = ''
    while 1:
        #data = s.recv(size).decode('ascii')
        #if data:
        #    full_data = full_data + data
        #    if data[-2:] == '\r\n':
        #        print(full_data)
        #        full_data = ''
        #        print('---')
        #while 1:
        text = input()
        if text == "quit":
            break
        s.send(bytes(text, 'UTF-8'))
        print("---Sent!---")
    s.close()
except:
    print("Closing socket")
    s.close()
