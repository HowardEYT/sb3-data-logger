from PyQt5.QtWidgets import QWidget, QMainWindow, QVBoxLayout, QTextEdit
from PyQt5.Qt import QTimer, QApplication
import queue, threading, serial, sys
import multiprocessing

class MainApp(QWidget):

    def __init__(self):
        super().__init__()


        self.ser = serial.Serial('/dev/ttyACM0', 9600)

        self.layout = QVBoxLayout(self)
        self.textbox = QTextEdit(self)
        self.layout.addWidget(self.textbox)

        self.queue = multiprocessing.Queue()
        self.running = True

        self.thread = multiprocessing.Process(target=self.workerThread)
        self.thread.start()

        self.update_timer = QTimer(self)
        self.update_timer.setInterval(1000)
        self.update_timer.timeout.connect(self.processIncoming)
        self.update_timer.start()

        self.show()

    def closeEvent(self, ev):
        self.running = False
        self.ser.close()
        self.thread.terminate()

    def workerThread(self):
        while self.running and self.ser.is_open:
            msg = self.ser.readline()
            if msg:
                self.queue.put(msg)
                print('Receive msg')
            else:
                print('No msg')

    def processIncoming(self):
        while self.queue.qsize():
            try:
                msg = self.queue.get(0).decode('ascii')
                self.textbox.insertPlainText(str(msg))
            except Queue.Empty:
                print('No msg')

if __name__ == "__main__":
    app = 0
    app = QApplication(sys.argv)

    root  = MainApp()

    sys.exit(app.exec_())
